package main

import (
    "fmt"
    "io/ioutil"
    "net/http"
    "flag"
)

func main() {
    var urlPtr = flag.String("url", "", "Url que Dobby va appeler pour vous.")
    var apiKeyPtr = flag.String("apikey", "", "ApiKey a utiliser pour appeler votre url.")
    flag.Parse()
    
    fmt.Println("Dobby pour vous servir...")
    // http://api.openweathermap.org/data/2.5/weather?q={city name},{state code},{country code}&appid={your api key}&units=metric
    // http://api.openweathermap.org/data/2.5/weather?q=Chamonix
    // 922e4321ed73e2c1c2bd6e3ac7a0715f
    fmt.Println(*urlPtr + "&appid=" + *apiKeyPtr + "&units=metric")
    response, err := http.Get(*urlPtr + "&appid=" + *apiKeyPtr + "&units=metric")
    if err != nil {
        fmt.Printf("La requête HTTP a échoué avec l'erreur %s\n", err)
    } else {
        data, _ := ioutil.ReadAll(response.Body)
        fmt.Println(string(data))
    }
    fmt.Println("Dobby reprend sa liberté")
}